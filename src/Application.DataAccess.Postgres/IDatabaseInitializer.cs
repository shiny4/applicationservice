﻿using DataAccess.Postgres;

namespace Application.DataAccess.Postgres
{
    public interface IDatabaseInitializer
    {
        void Initialize();
        void Initialize(DataContext dbContext);
        void AddContent(DataContext dbContext);
        void Refresh();
    }
}