﻿using Application.DataAccess.Postgres;
using DataAccess.Postgres;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.DataAccess.Postgres
{
    public class DatabaseInitializer : IDatabaseInitializer
    {
        private readonly DataContext _dataContext;

        public DatabaseInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public DatabaseInitializer()
        {
            _dataContext = null;
        }

        public void Initialize()
        {
            _dataContext.Database.EnsureDeleted();
                _dataContext.Database.EnsureCreated();
                AddContent(_dataContext);
        }

        public void Initialize(DataContext dbContext)
        {
            dbContext.Database.EnsureCreated();
            AddContent(dbContext);
        }

        public void AddContent(DataContext dbContext)
        {
            dbContext.Requests.AddRange(DataSeed.Requests);
            dbContext.Bets.AddRange(DataSeed.Bets);
            dbContext.SaveChanges();
        }
        
        public void Refresh()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();
        }
    }
}