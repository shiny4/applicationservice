﻿using System;
using System.Collections.Generic;
using Application.Entities.Models;

namespace Application.DataAccess.Postgres
{
    public class DataSeed
    {
        private static string _request1 = "1c7dab91-3317-41a5-adf5-8682fa3d12af";
        private static string _request2 = "b296fb4f-e1fa-401b-907f-f28a88f47cf6";
        private static string _request3 = "2ec1dcdc-b2b5-4a9e-8ace-33a8ae240f0f";
        private static string _request4 = "1989e6d9-997d-445a-8233-3f57ac61765b";

        private static string _student1 = "4122d412-682a-4bad-9837-ceda4824e261";
        private static string _student2 = "21092523-4d24-4909-b774-751de633ed95";

        private static string _teacherId1 = "2c73101f-d2d4-46e4-9e29-93459dc71bc7";
        private static string _teacherId2 = "0d3acdc7-ee94-4e22-8edc-aaaf92072eac";

        private static string _subject1 = "eeaeafc9-9b25-4001-9bd7-daaa621ccf7f";
        private static string _subject2 = "3b54ba34-9eb4-48a3-a8fc-60030bec750e";

        private static string _bet1 = "42028852-42bb-4f97-be98-f8b89a02b002";
        private static string _bet2 = "11028852-42bb-4f97-be98-f8b89a02b002";
        private static string _bet3 = "33028852-42bb-4f97-be98-f8b89a02b002";
        private static string _bet4 = "99028852-42bb-4f97-be98-f8b89a02b002";
        private static string _bet5 = "5c7176a5-1ec2-487a-85e7-b8c60f3b0490";

        public static IEnumerable<Request> Requests => new List<Request>()
        {
            new Request()
            {
                Id = Guid.Parse(_request1),
                StudentId = Guid.Parse(_student1),
                SubjectId = Guid.Parse(_subject2),
                TeacherId = Guid.Parse(_teacherId1),
                DesiredDateTime = DateTime.Now,
                Description = "I want to study English",
                ActuallUntil = DateTime.Now.AddDays(3),
                CreationDate = DateTime.Now.AddDays(1),
                Status = Entities.Enums.RequestStatus.Active,
                Price = 1900.50F
            },
            new Request()
            {
                Id = Guid.Parse(_request2),
                StudentId = Guid.Parse(_student2),
                SubjectId = Guid.Parse(_subject1),
                TeacherId = Guid.Parse(_teacherId2),
                DesiredDateTime = DateTime.Now,
                Description = "I want to study math",
                ActuallUntil = DateTime.Now.AddDays(6),
                CreationDate = DateTime.Now.AddDays(3),
                Status = Entities.Enums.RequestStatus.Active,
                Price = 300.2F
            },
            new Request()
            {
                Id = Guid.Parse(_request3),
                StudentId = Guid.Parse(_student2),
                SubjectId = Guid.Parse(_subject1),
                DesiredDateTime = DateTime.Now,
                Description = "I want to study qeometry",
                ActuallUntil = DateTime.Now.AddDays(6),
                CreationDate = DateTime.Now.AddDays(3),
                Status = Entities.Enums.RequestStatus.Active,
                Price = 300.2F
            },
            new Request()
            {
                Id = Guid.Parse(_request4),
                StudentId = Guid.Parse(_student2),
                SubjectId = Guid.Parse(_subject2),
                DesiredDateTime = DateTime.Now,
                Description = "I want to study biology",
                ActuallUntil = DateTime.Now.AddDays(6),
                CreationDate = DateTime.Now.AddDays(3),
                Status = Entities.Enums.RequestStatus.Active,
                Price = 300.2F
            }
        };

        public static IEnumerable<Bet> Bets = new List<Bet>()
        {
            new Bet()
            {
                Id = Guid.Parse(_bet1),
                RequestId = Guid.Parse(_request2),
                AuthorId = Guid.Parse(_teacherId1),
                Price = 654.89F,
                Description = "Готов поработать за такую сумму",
                CreationDate = DateTime.Now
            },
            new Bet()
            {
                Id = Guid.Parse(_bet2),
                RequestId = Guid.Parse(_request2),
                AuthorId = Guid.Parse(_teacherId1),
                Price = 3000.89F,
                Description = "Хотя лучше вот за эту сумму",
                CreationDate = DateTime.Now
            },
            new Bet()
            {
                Id = Guid.Parse(_bet3),
                RequestId = Guid.Parse(_request2),
                AuthorId = Guid.Parse(_teacherId1),
                Price = 2000.89F,
                Description = "Это слишком дорого",
                CreationDate = DateTime.Now
            },
            new Bet()
            {
                Id = Guid.Parse(_bet4),
                RequestId = Guid.Parse(_request2),
                AuthorId = Guid.Parse(_teacherId1),
                Price = 2500.89F,
                Description = "Мое последнее предложение",
                CreationDate = DateTime.Now
            },
            new Bet()
            {
                Id = Guid.Parse(_bet5),
                RequestId = Guid.Parse(_request1),
                AuthorId = Guid.Parse(_teacherId2),
                Price = 1200F,
                Description = "Готов поработать за такую сумму",
                CreationDate = DateTime.Now
            }
        };
    }
}