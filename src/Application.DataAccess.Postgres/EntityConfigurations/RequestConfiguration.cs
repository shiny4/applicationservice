﻿using Application.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.DataAccess.Postgres.EntityConfigurations
{
    public class RequestConfiguration : IEntityTypeConfiguration<Request>
    {
        public void Configure(EntityTypeBuilder<Request> builder)
        {
            builder.ToTable("Requests");

            builder.HasKey(r => r.Id);

            builder.Property(r => r.Subject).HasMaxLength(100);
            builder.Property(r => r.StudentId).IsRequired();
            builder.Property(r => r.Description).IsRequired().HasMaxLength(200);
            builder.Property(r => r.DesiredDateTime).IsRequired();
            builder.Property(r => r.CreationDate).IsRequired();
            builder.Property(r => r.ActuallUntil).IsRequired();
        }
    }
}