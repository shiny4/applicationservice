﻿using Application.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Application.DataAccess.Postgres.EntityConfigurations
{
    public class BetConfiguration : IEntityTypeConfiguration<Bet>
    {
        public void Configure(EntityTypeBuilder<Bet> builder)
        {
            builder.ToTable("Bets");

            builder.HasKey(r => r.Id);

            builder.Property(r => r.RequestId).IsRequired();
            builder.Property(r => r.AuthorId).IsRequired();
            builder.Property(r => r.Price).IsRequired();
            builder.Property(r => r.Description).HasMaxLength(200);
        }
    }
}
