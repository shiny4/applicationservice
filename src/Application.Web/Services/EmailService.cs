using System;
using System.Threading.Tasks;
using Application.Infrastructure.Interfaces.OutsideModels;
using Application.Infrastructure.Interfaces.Services;
using Jumping.Contracts;
using MassTransit;
using MassTransit.Contracts;
using Microsoft.Extensions.Logging;

namespace Application.Web.Services
{
    public class EmailService : IEmailService
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly ILogger<EmailService> _logger;
        private readonly IPublishEndpoint _publishEndpoint;

        public EmailService(ICurrentUserService currentUserService, ILogger<EmailService> logger, IPublishEndpoint publishEndpoint)
        {
            _currentUserService = currentUserService;
            _logger = logger;
            _publishEndpoint = publishEndpoint;
        }

        public async Task SendEmailAsync(string subject, string body,string address="")
        {
            if (string.IsNullOrWhiteSpace(address)) address = _currentUserService.Email;
            if (string.IsNullOrWhiteSpace(address)) return;
            if (string.IsNullOrWhiteSpace(subject)) return;

            try
            {
                await _publishEndpoint.Publish(new EmailNotification
                {
                    Address = address,
                    Subject = subject,
                    Body = $"уважаемый {_currentUserService.Name}. {body}"
                });
                _logger.LogInformation("Try to sent mail for adress='{EmailTo}' with subject='{MailSubject}'",
                    address, subject);
            }
            catch (Exception ex)
            {
                _logger.LogError("Mail for adress='{EmailTo}' with subject='{MailSubject}' was not sent. Error: {MailError} ",
                    address, subject, ex.Message);
            }
        }

        // await _publishEndpoint.Publish(new EmailNotification
        // {
        //     Address = "Lexs200610@yandex.ru",
        //     Subject = "Новая заявка от Jumping Study Platform",
        //     Body = "Поздравляю, " + data.Message.FirstName + " " + data.Message.LastName + "! Вы успешно создали заявку, скоро преподаватели откликнутся на нее и долгожданное обучение начнется=)"
        // });
        //await _publishEndpoint.Publish(new EmailNotification
        //{
        //    Address = "Lexs200610@yandex.ru",
        //    Subject = "Новая заявка от Jumping Study Platform",
        //    Body = "Поздравляю! Вы успешно создали заявку, скоро преподаватели откликнутся на нее и долгожданное обучение начнется=)"
        //});

        
    }
}