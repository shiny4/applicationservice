﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Entities.Models;
using Application.Infrastructure.Interfaces;
using Application.UseCases.Handlers.Requests.Dto;
using Application.UseCases.Handlers.Requests.Interfaces;
using Application.Web.Utils;
using AutoMapper;

namespace Application.Web.Services
{
    public class RequestService : IRequestService
    {
        private readonly IRequestRepository _requestRepository;
        private readonly IMapper _mapper;

        public RequestService(IRequestRepository requestRepository, IMapper mapper)
        {
            _requestRepository = requestRepository;
            _mapper = mapper;
        }

        public async Task<string> AddRequestAsync(CreateOrUpdateRequestDto request)
        {
            if (request is null)
            {
                throw new AppException($"Request cannot be empty", 400);
            }

            var newRequest = _mapper.Map<Request>(request);

            var newId = await _requestRepository.CreateAsync(newRequest);

            return $"Request was successfuly created with id {newId}.";
        }

        public async Task<ICollection<RequestResponseDto>> GetAllRequests()
        {
            var requests = await _requestRepository.GetAllAsync();

            return _mapper.Map<List<RequestResponseDto>>(requests);
        }

        public async Task<RequestResponseDto> GetRequestByID(Guid id)
        {
            var admin = await _requestRepository.GetByIdAsync(id);
            return _mapper.Map<RequestResponseDto>(admin);
        }

        public async Task RemoveRequestAsync(Guid id)
        {
            var request = await _requestRepository.GetByIdAsync(id);

            if (request == null)
            {
                throw new AppException($"Request with {id} wasn't found", 500);
            }

            await _requestRepository.DeleteAsync(request);
        }

        public async Task UpdateRequestAsync(Guid id, CreateOrUpdateRequestDto request)
        {
            if (request is null)
            {
                throw new AppException($"Request cannot be empty", 400);
            }

            var exsistingRequest = await _requestRepository.GetRequestForUpdateAsync(id);

            if (exsistingRequest is null)
            {
                throw new AppException($"Request with {id} doesn't exsists", 400);
            }
            
            var updateRequest = _mapper.Map(request, exsistingRequest);

            await _requestRepository.UpdateAsync(updateRequest);
        }
    }
}