﻿using System;
using Microsoft.EntityFrameworkCore;
using Application.Entities.Models;
using Application.Infrastructure.Interfaces;
using DataAccess.Postgres;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace Application.Web.Services
{
    public class RequestRepository : BaseRepository<Request>, IRequestRepository
    {
        public RequestRepository(DataContext context) : base(context)
        {

        }

        public async Task<Request> GetDetailsRequestAsync(Guid id)
        {
            return await DbSet.AsNoTracking()
                              .Where(r => r.Id == id)
                              .Include(r => r.Bets.OrderBy(b => b.CreationDate))
                              .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Request>> GetOpenedAuctions()
        {
            return await DbSet.Where(r => r.TeacherId == null && r.Status == Entities.Enums.RequestStatus.Active).ToListAsync();
        }

        public async Task<Request> GetRequestForUpdateAsync(Guid id)
        {
            return await DbSet.FirstOrDefaultAsync(r => r.Id == id);
        }

        public async Task<ICollection<Request>> GetRequestsForStudent(Guid Id)
        {
            return await DbSet.AsNoTracking().Where(x => x.StudentId == Id).ToListAsync();
        }
    }
}
