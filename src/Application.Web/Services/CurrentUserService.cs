using System;
using System.Security.Claims;
using Application.Infrastructure.Interfaces.Services;
using Microsoft.AspNetCore.Http;

namespace Application.Web.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        private readonly HttpContext _httpContext;
       
        public CurrentUserService(IHttpContextAccessor httpContextAccessor)
        {
            if (httpContextAccessor != null)
            {
                _httpContext = httpContextAccessor.HttpContext;
                if (_httpContext != null && _httpContext.User.Identity != null)
                {
                    IsAuthenticated = _httpContext.User.Identity.IsAuthenticated;
                    if (IsAuthenticated)
                    {
                        Guid.TryParse(_httpContext.User.FindFirstValue(ClaimTypes.Sid), out var userId);
                        UserId = userId;
                        Email = _httpContext.User.FindFirstValue(ClaimTypes.Email);
                        Role = _httpContext.User.FindFirstValue(ClaimTypes.Role);
                        Name = _httpContext.User.FindFirstValue(ClaimTypes.Name);
                    }
                    else
                    {
                       
                    }
                }
            }
        }

        public Guid UserId { get; } = Guid.Empty;

        public bool IsAuthenticated { get; } = false;

        public string Email { get; } = "";

        public string Name { get; } = "";

        public string Role { get; } = "";
    }
}