﻿using System;
using Microsoft.EntityFrameworkCore;
using Application.Entities.Models;
using Application.Infrastructure.Interfaces;
using DataAccess.Postgres;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace Application.Web.Services
{
    public class BetRepository : BaseRepository<Bet>, IBetRepository
    {
        public BetRepository(DataContext context) : base(context)
        {

        }

        public async Task<Bet> GetBetForUpdateAsync(Guid id)
        {
            return await DbSet.FirstOrDefaultAsync(r => r.Id == id);
        }

        public async Task<ICollection<Bet>> GetBetsForRequest(Guid Id)
        {
            return await DbSet.AsNoTracking().Where(x => x.RequestId == Id).ToListAsync();
        }
    }
}
