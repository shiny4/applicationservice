﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.UseCases.Handlers.Bets.Interfaces;
using Application.UseCases.Handlers.Bets.Dto;
using Application.Entities.Models;
using Application.Web.Utils;
using Application.Infrastructure.Interfaces;

namespace Application.Web.Services
{
    public class BetService : IBetService
    {
        private readonly IBetRepository _betRepository;
        private readonly IMapper _mapper;

        public BetService(IBetRepository betRepository, IMapper mapper)
        {
            _betRepository = betRepository;
            _mapper = mapper;
        }

        public async Task<string> AddBetAsync(CreateOrUpdateBetDto bet)
        {
            if (bet is null)
            {
                throw new AppException($"Request cannot be empty", 400);
            }

            var newBet = _mapper.Map<Bet>(bet);

            var newId = await _betRepository.CreateAsync(newBet);

            return $"Bet was successfuly created with id {newId}.";
        }

        public async Task<BetResponseDto> GetBetByID(Guid id)
        {
            var bet = await _betRepository.GetByIdAsync(id);
            return _mapper.Map<BetResponseDto>(bet);
        }

        public async Task RemoveBetAsync(Guid id)
        {
            var bet = await _betRepository.GetByIdAsync(id);

            if (bet == null)
            {
                throw new AppException($"Bet with {id} wasn't found", 500);
            }

            await _betRepository.DeleteAsync(bet);
        }

        public async Task UpdateBetAsync(Guid id, CreateOrUpdateBetDto bet)
        {
            if (bet is null)
            {
                throw new AppException($"Bet cannot be empty", 400);
            }

            var exsistingBet = await _betRepository.GetBetForUpdateAsync(id);

            if (exsistingBet is null)
            {
                throw new AppException($"Bet with {id} doesn't exsists", 400);
            }

            var updateBet = _mapper.Map(bet, exsistingBet);

            await _betRepository.UpdateAsync(updateBet);
        }
    }
}