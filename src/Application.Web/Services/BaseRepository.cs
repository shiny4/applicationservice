﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Application.Entities;
using Application.Infrastructure.Interfaces;
using DataAccess.Postgres;
using Microsoft.EntityFrameworkCore;

namespace Application.Web.Services
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity>
             where TEntity : BaseEntity
    {
        
        protected DataContext Context { get; }

        protected DbSet<TEntity> DbSet { get; }

        public BaseRepository(DataContext context)
        {
            Context = context;
            DbSet = Context?.Set<TEntity>();
        }

        public async Task<ICollection<TEntity>> GetAllAsync()
        {
            return await DbSet.AsNoTracking().ToListAsync();
        }

        public async Task<ICollection<TEntity>> GetAsync(Expression<Func<TEntity, bool>> predicate, int maxItemsAmount = 0)
        {
            var query = DbSet.AsNoTracking().Where(predicate);
            query = maxItemsAmount > 0 ? query.Take(maxItemsAmount) : query;

            return await query.ToListAsync();
        }

        public async Task<TEntity> GetByIdAsync(Guid id)
        {
            return await DbSet.AsNoTracking().SingleOrDefaultAsync(e => e.Id == id);
        }

        public async Task<Guid> CreateAsync(TEntity entity)
        {
            await DbSet.AddAsync(entity);

            await SaveChanges();

            return entity.Id;
        }

        public async Task CreateBulkAsync(ICollection<TEntity> entities)
        {
            await DbSet.AddRangeAsync(entities);

            await SaveChanges();
        }

        public async Task UpdateAsync(TEntity item)
        {
            await SaveChanges();
        }

        public async Task UpdateBulkAsync(ICollection<TEntity> items)
        {
            try
            {
                //disable detection of changes to improve performance
                Context.ChangeTracker.AutoDetectChangesEnabled = false;

                //then perform the update
                await SaveChanges();
            }
            finally
            {
                //re-enable detection of changes
                Context.ChangeTracker.AutoDetectChangesEnabled = true;
            }
        }

        public async Task DeleteAsync(TEntity item)
        {
            DbSet.Remove(item);

            await SaveChanges();
        }

        public async Task DeleteBulkAsync(ICollection<TEntity> items)
        {
            DbSet.RemoveRange(items);

            await SaveChanges();
        }

        //public async Task<IQueryable<TEntity>> S

        private async Task SaveChanges()
        {
            await Context.SaveChangesAsync();
        }
    }
}
