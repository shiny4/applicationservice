﻿using Application.UseCases.Handlers.Requests.Commands;
using Application.UseCases.Handlers.Requests.Dto;
using Application.UseCases.Handlers.Requests.Interfaces;
using Application.UseCases.Handlers.Requests.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;

namespace Application.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CrudRequestController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CrudRequestController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _mediator.Send(new GetAllRequestsQuery()));
        }

        [HttpGet("{id}")]
        [Authorize]
        public async Task<IActionResult> GetById(Guid id)
        {
            return Ok(await _mediator.Send(new GetRequestByIdQuery { Id = id }));
        }
        
        [Authorize(Roles = "Student, Both,Admin")]
        [HttpPost]
        public async Task<IActionResult> Create(CreateRequestCommand command)
        {
            return Ok(await _mediator.Send(command));
        }
        
        [Authorize(Roles = "Student, Both,Admin")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(Guid id, UpdateRequestCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return Ok(await _mediator.Send(command));
        }

        [Authorize(Roles = "Student, Both,Admin")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            return Ok(await _mediator.Send(new DeleteRequestByIdCommand { Id = id }));
        }
    }
}

//У: Просмотреть детальную информацию по встречной заявке от преподавателя

//jumping JUM-108	
//У: Отправить сообщение преподавателю

//jumping JUM-107	
//У: Подтвердить заявку

//jumping JUM-106	
//У: Посмотреть профиль преподавателя

//jumping JUM-105	
//У: Просмотреть список откликнувшихся преподавателей

//jumping JUM-104	
//У: Отменить аукцион

//jumping JUM-103	
//У: Просмотреть детальную информацию по заявке с аукциона

//jumping JUM-102	
//У: Просмотреть список своих аукционов

//jumping JUM-101	
//У: Открыть новый аукцион на занятие

//jumping JUM-100	
//Пр: Выставить встречную заявку

//jumping JUM-99	
//Пр: Отправить сообщение автору заявки

//jumping JUM-98	
//Пр: Подтвердить заявку

//jumping JUM-97	
//Пр: Посмотреть профиль автора заявки

//jumping JUM-96	
//Пр: Просмотреть детальную информацию по заявке в аукционе

//jumping JUM-95	
//Пр: Посмотреть список активных аукционов на занятие

//jumping JUM-94	
//Работа с аукционами

//jumping JUM-93	
//Пз: Просмотреть список входящих заявок от других участников

//jumping JUM-92	
//Пз: Посмотреть профиль автора заявки

//jumping JUM-91	
//Пз: Подтвердить заявку

//jumping JUM-90	
//Пз: Отправить сообщение автору заявки

//jumping JUM-89	
//Пз: Выставить встречную заявку

//jumping JUM-88	
//Пз: Отменить заявку

//jumping JUM-87	
//Пз: Просмотреть детальную информацию по заявке

//jumping JUM-86	
//Пз: Просмотреть список своих (исходящих) заявок