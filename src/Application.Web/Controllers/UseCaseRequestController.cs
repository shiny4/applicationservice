﻿using Application.UseCases.Handlers.Requests.Commands;
using Application.UseCases.Handlers.Requests.Dto;
using Application.UseCases.Handlers.Requests.Interfaces;
using Application.UseCases.Handlers.Requests.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Infrastructure.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using NSwag.Annotations;
using Microsoft.Extensions.DependencyInjection;

namespace Application.Web.Controllers
{
    /// <summary>
    /// Управление заявками. Пользовательские сценарии.
    /// </summary>
    [Route("api/v1/[controller]")]
    [ApiController]
    public class UseCaseRequestController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ICurrentUserService _currentUserService;
        public UseCaseRequestController(IMediator mediator, ICurrentUserService currentUserService)
        {
            _mediator = mediator;
            _currentUserService = currentUserService;
        }

        /// <summary>
        /// Принять заявку преподавателем
        /// </summary>
        /// <param name="Requestid">Id заявки</param>
        /// <param name="TeacherId">Id преподавателя</param>
        /// <returns></returns>
        [Authorize(Roles = "Teacher, Both,Admin")]
        [HttpPost("TakeRequestByTeacher")]
        public async Task<IActionResult> TakeRequestByTeacher(Guid Requestid, Guid TeacherId)
        {
            return Ok(await _mediator.Send(new TakeRequestByTeacherCommand { RequestId = Requestid, TeacherId = TeacherId }));
        }

        /// <summary>
        ///  Посмотреть список заявок пользователя
        /// </summary>
        /// <param name="id">Id студента</param>
        /// <returns>Список заявок</returns>
        [Authorize]
        [HttpGet("GetRequestsForStudent/{id}")]
        public async Task<IActionResult> GetRequestsForStudent(Guid id)
        {
            return Ok(await _mediator.Send(new GetRequestsForStudentQuery{ StudentId = id }));
        }

        /// <summary>
        ///  Посмотреть список своих заявок
        /// </summary>
        /// <param name="StudentId">Id студента</param>
        /// <returns>Список заявок</returns>
        [Authorize]
        [HttpGet("GetMyRequests")]
        public async Task<IActionResult> GetMyRequests()
        {
            var StudentId = _currentUserService.UserId;
            return Ok(await _mediator.Send(new GetMyRequestsQuery { StudentId = StudentId }));
        }

        /// <summary>
        /// Получить детальную информацию по заявке
        /// </summary>
        /// <param name="RequestId">Id студента</param>
        /// <returns>Список заявок</returns>
        [Authorize]
        [HttpGet("GetRequestWithDetailedInfo/{RequestId}")]
        public async Task<IActionResult> GetRequestWithDetailedInfo(Guid RequestId)
        {
            return Ok(await _mediator.Send(new GetRequestWithDetailedInfoQuery { RequestId = RequestId }));
        }

        /// <summary>
        ///  Получить открытые аукционы
        /// </summary>
        /// <returns>Список открытых аукционов</returns>
        [HttpGet("GetOpenedAuctions")]
        public async Task<IActionResult> GetOpenedAuctions()
        {
            return Ok(await _mediator.Send(new GetOpenedAuctions { }));
        }

        /// <summary>
        ///  Отменить заявку
        /// </summary>
        /// <param name="RequestId">Id заявки</param>
        /// <param name="StudentId">Id студента</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("CancelRequestCommand")]
        public async Task<IActionResult> CancelRequestCommand(Guid RequestId)
        {
            var StudentId = _currentUserService.UserId;
            return Ok(await _mediator.Send(new CancelRequestCommand { RequestId = RequestId, StudentId = StudentId }));
        }
    }
}

//У: Просмотреть детальную информацию по встречной заявке от преподавателя -----> Deprecated

//У: Отправить сообщение  ------->  New Microservice of Messages

//У: Посмотреть профиль преподавателя ------> Microservice of User: https://ms-user/teacher/id

//У: Просмотреть список откликнувшихся преподавателей/

//У: Отменить аукцион

//У: Просмотреть детальную информацию по заявке с аукциона

//У: Просмотреть список своих аукционов

//У: Открыть новый аукцион на занятие

//Пр: Выставить встречную заявку   ----- Deprecated

//Пр: Отправить сообщение автору заявки ------->  New Microservice of Messages

//Пр: Посмотреть профиль автора заявки ------> Microservice of User: https://ms-user/student/id

//Пр: Просмотреть детальную информацию по заявке в аукционе

//Пр: Посмотреть список активных аукционов на занятие

//Пз: Просмотреть список входящих заявок от других участников  ----- Deprecated

//Пз: Посмотреть профиль автора заявки ------> Microservice of User: https://ms-user/student/id

//Пз: Отправить сообщение автору заявки ------->  New Microservice of Messages

//Пз: Выставить встречную заявку ----- Deprecated: Создавать заявки может только авторизованный пользователь