﻿using Application.UseCases.Handlers.Bets.Commands;
using Application.UseCases.Handlers.Bets.Dto;
using Application.UseCases.Handlers.Bets.Interfaces;
using Application.UseCases.Handlers.Bets.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;

namespace Application.Web.Controllers
{
    /// <summary>
    /// Управление заявками. Пользовательские сценарии.
    /// </summary>
    
    [Route("api/[controller]")]
    [ApiController]
    public class BetController : ControllerBase
    {
        private readonly IMediator _mediator;

        public BetController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Получить ставку по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(Guid id)
        {
            return Ok(await _mediator.Send(new GetBetByIdQuery { Id = id }));
        }

        /// <summary>
        /// Создать ставку 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [Authorize(Roles = "Student, Both,Admin")]
        [HttpPost]
        public async Task<IActionResult> Create(CreateBetCommand command)
        {
            return Ok(await _mediator.Send(command));
        }

        /// <summary>
        /// Редактировать ставку
        /// </summary>
        /// <param name="id"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(Guid id, UpdateBetCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return Ok(await _mediator.Send(command));
        }

        /// <summary>
        /// Удалить ставку
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            return Ok(await _mediator.Send(new DeleteBetByIdCommand { Id = id }));
        }

        /// <summary>
        /// Принять ставку
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("/acceptbet/{id}")]
        public async Task<IActionResult> AcceptBet(Guid id)
        {
            return Ok(await _mediator.Send(new AcceptBetCommand { Id = id }));
        }
    }
}
