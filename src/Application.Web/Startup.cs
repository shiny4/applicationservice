using System.Linq;
using System.Text;
using Application.DataAccess.Postgres;
using Application.Infrastructure.Interfaces;
using Application.Infrastructure.Interfaces.Services;
using Application.UseCases.Behaviours;
using Application.UseCases.Handlers.Bets.Interfaces;
using Application.UseCases.Handlers.Bets.Mappings;
using Application.UseCases.Handlers.Requests.Interfaces;
using Application.UseCases.Handlers.Requests.Mappings;
using Application.Web.Options;
using Application.Web.Services;
using Application.Web.Utils;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using DataAccess.Postgres;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using NSwag;
using Serilog;

namespace Application.Web
{
    public class Startup
    {
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
        private JWTOptions _jwtOptions;
        //public ILifetimeScope AutofacContainer { get; private set; }
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
            _jwtOptions = this.Configuration
                .GetSection("JWTOptions")
                .Get<JWTOptions>();
        }
        // public void ConfigureContainer(ContainerBuilder builder)
        // {
        //     builder.RegisterModule(new AutofacConfigurationModule());
        // }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.Configure<JWTOptions>(Configuration.GetSection("JWTOptions"));
            services.AddControllersWithViews();
            services.AddHttpContextAccessor();
            services.AddAutoMapper(typeof(RequestMapping));
            services.AddAutoMapper(typeof(BetMapping));
            services.AddDbContext<DataContext>(options =>
            {
                //options.UseSqlite(Configuration.GetConnectionString("SqlConnection"));
                options.UseNpgsql(Configuration.GetConnectionString("ApplicationDb"),
                    x => x.MigrationsAssembly("Application.Web"));
            });
            services.AddScoped<IDatabaseInitializer, DatabaseInitializer>();
            services.AddMediatR(typeof(ValidationBehaviour<,>).Assembly);
            services.AddValidatorsFromAssembly(typeof(ValidationBehaviour<,>).Assembly);
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(LoggingBehaviour<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehaviour<,>));
            
            services.AddScoped<IRequestRepository, RequestRepository>();
            services.AddScoped<IBetRepository, BetRepository>();
            
            services.AddScoped<IRequestService, RequestService>();
            services.AddScoped<IBetService, BetService>();
            services.AddScoped<ICurrentUserService, CurrentUserService>();
            services.AddScoped<IEmailService, EmailService>();
            
            services.AddScoped<INotificationService, NotificationServiceClient>();

            services.AddOpenApiDocument(options =>
            {
                options.Title = "Request MicroService Doc";
                options.Version = "1.0";   
                options.AddSecurity("JWT", Enumerable.Empty<string>(), new OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.ApiKey,
                    Name = "Authorization",
                    In = OpenApiSecurityApiKeyLocation.Header,
                    Description = "Type into the textbox: Bearer {your JWT token}."
                });
            });
            
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = false,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = _jwtOptions.Issuer,
                    ValidAudience = _jwtOptions.Audience,
                    IssuerSigningKey =
                        new SymmetricSecurityKey(
                            Encoding.ASCII.GetBytes(_jwtOptions.Secret))
                };
            });

            services.ConfigureServices(Configuration);
            //ConfigureServicesMassTransit.ConfigureServices(services, configuration);
            
            
            
            services.AddCors(options =>
            {
                options.AddPolicy(name: MyAllowSpecificOrigins,
                    builder =>
                    {
                        builder.WithOrigins("http://localhost:3000")
                               .AllowAnyMethod()
                               .AllowAnyHeader();
                    });
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app,
            IWebHostEnvironment env, 
            DataContext dataContext,
            IConfiguration configuration,
            IDatabaseInitializer dbInitializer)
        {
            // AutofacContainer = app.ApplicationServices.GetAutofacRoot();
            app.UseSerilogRequestLogging();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseMiddleware<ExceptionHandlerMiddleware>();
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            
            if (configuration.GetValue<bool>("recreateDB") == true) dbInitializer.Refresh();
            if (configuration.GetValue<bool>("InitializeDB") == true) dbInitializer.Initialize();
            
            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            
            app.UseCors(MyAllowSpecificOrigins);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
