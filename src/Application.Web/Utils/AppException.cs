﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Web.Utils
{
    public class AppException : Exception
    {
        public int? StatusCode { get; set; }

        public AppException()
        {
        }

        public AppException(string message, int statusCode) : base(message)
        {
            StatusCode = statusCode;
        }

        public AppException(string message, Exception exception)
            : base(message, exception)
        {

        }

        public AppException(string message, int statusCode, Exception exception)
           : base(message, exception)
        {
            StatusCode = statusCode;
        }
    }
}