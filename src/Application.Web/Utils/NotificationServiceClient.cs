﻿using System;
using Application.Infrastructure.Interfaces.Services;
using Microsoft.Extensions.Configuration;
using RestSharp;

namespace Application.Web.Utils
{
    public class NotificationServiceClient: RestClient, INotificationService
    {
        public IConfiguration Configuration { get; }

        public NotificationServiceClient(IConfiguration configuration)
        {
            Configuration = configuration;
            this.BaseUrl = new Uri(Configuration.GetSection("RestApiSection:HotificationServiceHost").Value);
        }
    }
}
