using System.Reflection;
using Application.Web.Services;
using Autofac;
using Module = Autofac.Module;


namespace Application.Web.Utils
{
    public class AutofacConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
           // var repositories = typeof(BaseRepository<>).Assembly;
            builder.RegisterAssemblyTypes(typeof(BaseRepository<>).Assembly)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(typeof(BaseRepository<>).Assembly)
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}