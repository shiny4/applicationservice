using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Application.UseCases.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Application.Web.Utils
{
    public class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<Program> _logger;

        public ExceptionHandlerMiddleware(RequestDelegate next, ILogger<Program> logger)
        {
            _next = next;
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                var response = FindException(ex);
                if (response.Status != null)
                {
                    context.Response.StatusCode = response.Status.Value;
                    context.Response.ContentType = "application/json";
                }

                await context.Response.WriteAsync(JsonConvert.SerializeObject(response));
            }
        }

        private ExceptionResponse FindException(Exception ex)
        {
            if (ex is AppException appException)
            {
                _logger.LogWarning("Error in {source} : {message}", appException.Source, appException.Message);
                return new ExceptionResponse
                {
                    Content = appException.Message,
                    Status = appException.StatusCode
                };
            }
            else if (ex is ApplicationValidationException applicationExceptionException)
            {
                _logger.LogWarning("Validation failed for {Request}. Error: {Error}",
                    applicationExceptionException.RequestName, applicationExceptionException.Errors);
                return new ValidationExceptionResponse
                {
                    Content = applicationExceptionException.Message,
                    Status = applicationExceptionException.StatusCode,
                    Errors = applicationExceptionException.Errors
                };
            }

            if (ex is UseCasesException useCasesException)
            {
                _logger.LogWarning("Error in {source} : {message}", useCasesException.Source, useCasesException.Message);
                return new ExceptionResponse
                {
                    Content = useCasesException.Message,
                    Status = useCasesException.StatusCode
                };
            }
            else
            {
                _logger.LogError("Error in {source}: {message}", ex.Source, ex.Message);
                return new ExceptionResponse
                {
                    Content = ex.Message,
                    Status = (int) HttpStatusCode.InternalServerError
                };
            }
        }

        public class ExceptionResponse
        {
            public int? Status { get; set; }
            public string Content { get; set; }
        }

        public class ValidationExceptionResponse : ExceptionResponse
        {
            public List<ApplicationValidationException.ErrorValue> Errors { get; set; }
        }
    }
}