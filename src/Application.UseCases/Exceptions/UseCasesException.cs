using System;

namespace Application.UseCases.Exceptions
{
    public class UseCasesException : Exception
    {
        public int? StatusCode { get; set; } = 400;

        public UseCasesException()
        {
        }
 
        public UseCasesException(string message)
            : base(message)
        {
        }

        public UseCasesException(string message, int statusCode)
            : base(message)
        {
            StatusCode = statusCode;
        }

        public UseCasesException(string message, Exception exception)
            : base(message, exception)
        {
        }

        public UseCasesException(string message, int statusCode, Exception exception)
            : base(message, exception)
        {
            StatusCode = statusCode;
        }
    }
}