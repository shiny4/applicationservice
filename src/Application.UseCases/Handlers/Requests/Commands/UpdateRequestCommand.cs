﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Entities.Enums;
using Application.Infrastructure.Interfaces;
using Application.Infrastructure.Interfaces.Services;
using Application.UseCases.Exceptions;
using FluentValidation;
using MediatR;

namespace Application.UseCases.Handlers.Requests.Commands
{
    public class UpdateRequestCommandValidator : AbstractValidator<UpdateRequestCommand>
    {
        public UpdateRequestCommandValidator()
        {
            RuleFor(p => p.StudentId)
                .NotEmpty().WithMessage("{PropertyName} is required.");

            RuleFor(p => p.SubjectId)
                .NotEmpty().WithMessage("{PropertyName} is required.");
            
            RuleFor(p => p.TeacherId)
                .NotEmpty().WithMessage("{PropertyName} is required.");
            
            RuleFor(p => p.Description)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull().WithMessage("{PropertyName} is required.")
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.")
                .MinimumLength(3).WithMessage("{PropertyName} must exceed 2 characters.");
            
            RuleFor(p => p.Subject)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull().WithMessage("{PropertyName} is required.")
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.")
                .MinimumLength(3).WithMessage("{PropertyName} must exceed 2 characters.");
            
            RuleFor(p => p.CreationDate)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .GreaterThan(DateTime.Now).WithMessage($"{{PropertyName}} must be greater {DateTime.Now}");
            
            RuleFor(p => p.DesiredDateTime)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .GreaterThan(DateTime.Now).WithMessage($"{{PropertyName}} must be greater {DateTime.Now}");
            
            RuleFor(p => p.ActuallUntil)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .GreaterThan(DateTime.Now).WithMessage($"{{PropertyName}} must be greater {DateTime.Now}");
        }
    }

    public class UpdateRequestCommand : IRequest<Guid>
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public string Subject { get; set; }
        public Guid SubjectId { get; set; }
        public Guid StudentId { get; set; }
        public Guid TeacherId { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ActuallUntil { get; set; }
        public DateTime DesiredDateTime { get; set; }
        public RequestStatus Status { get; set; }

        public class UpdateRequestCommandHandler : IRequestHandler<UpdateRequestCommand, Guid>
        {
            private readonly IRequestRepository _requestRepository;
            private readonly IEmailService _emailService;

            public UpdateRequestCommandHandler(IRequestRepository requestRepository, IEmailService emailService)
            {
                _requestRepository = requestRepository;
                _emailService = emailService;
            }
            public async Task<Guid> Handle(UpdateRequestCommand command, CancellationToken cancellationToken)
            {
                var request = await _requestRepository.GetRequestForUpdateAsync(command.Id);
                if (request == null)
                {
                    throw new UseCasesException($"Request with {command.Id} wasn't found", 404);
                }
                else
                {
                    request.CreationDate = DateTime.Now;
                    request.Description = command.Description;
                    request.DesiredDateTime = command.DesiredDateTime;
                    request.SubjectId = command.SubjectId;
                    request.TeacherId = command.TeacherId;
                    request.StudentId = command.StudentId;
                    request.Subject = command.Subject;
                    request.ActuallUntil = request.DesiredDateTime.AddMonths(1);
                    request.Status = command.Status;

                    await _requestRepository.UpdateAsync(request);
                    await _emailService.SendEmailAsync("Редактирование заявки",
                        $"Заявка с ид={request.Id} изменена" );
                    return request.Id;
                }
            }
        }
    }
}
