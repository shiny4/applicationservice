﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Infrastructure.Interfaces;
using Application.Infrastructure.Interfaces.Services;
using Application.UseCases.Exceptions;
using FluentValidation;
using MediatR;

namespace Application.UseCases.Handlers.Requests.Commands
{
    public class DeleteRequestByIdCommandValidator : AbstractValidator<DeleteRequestByIdCommand>
    {
        public DeleteRequestByIdCommandValidator()
        {
            RuleFor(p => p.Id)
                .NotEmpty().WithMessage("{PropertyName} is required.");
        }
    }
    public class DeleteRequestByIdCommand : IRequest<Guid>
    {
        public Guid Id { get; set; }

        public class DeleteRequestByIdCommandHandler : IRequestHandler<DeleteRequestByIdCommand, Guid>
        {
            private readonly IRequestRepository _requestRepository;
            private readonly IEmailService _emailService;

            public DeleteRequestByIdCommandHandler(IRequestRepository requestRepository, IEmailService emailService)
            {
                _requestRepository = requestRepository;
                _emailService = emailService;
            }
            public async Task<Guid> Handle(DeleteRequestByIdCommand command, CancellationToken cancellationToken)
            {
                var request = await _requestRepository.GetByIdAsync(command.Id);

                if (request == null) throw new UseCasesException($"Request with {command.Id} wasn't found", 404);
                await _requestRepository.DeleteAsync(request);
                await _emailService.SendEmailAsync("Удаление заявки",
                    $"Заявка с ид={request.Id} удалена" );
                return request.Id;
            }
        }
    }
}
