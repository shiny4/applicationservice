﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading;
using System.Threading.Tasks;
using Application.Entities.Enums;
using Application.Entities.Models;
using Application.Infrastructure.Interfaces;
using MassTransit.Contracts;
using Application.Infrastructure.Interfaces.OutsideModels;
using Application.Infrastructure.Interfaces.Services;
using FluentValidation;
using Jumping.Contracts;
using MassTransit;
using MediatR;
using RestSharp;

namespace Application.UseCases.Handlers.Requests.Commands
{
    public class CreateRequestCommandValidator : AbstractValidator<CreateRequestCommand>
    {
        public CreateRequestCommandValidator()
        {
            RuleFor(p => p.StudentId)
                .NotEmpty().WithMessage("{PropertyName} is required.");

            RuleFor(p => p.SubjectId)
                .NotEmpty().WithMessage("{PropertyName} is required.");
            
            RuleFor(p => p.Description)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull().WithMessage("{PropertyName} is required.")
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.")
                .MinimumLength(3).WithMessage("{PropertyName} must exceed 2 characters.");
            
            RuleFor(p => p.Subject)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull().WithMessage("{PropertyName} is required.")
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.")
                .MinimumLength(3).WithMessage("{PropertyName} must exceed 2 characters.");
            
            RuleFor(p => p.CreationDate)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .GreaterThan(DateTime.Now).WithMessage($"{{PropertyName}} must be greater {DateTime.Now}");
            
            RuleFor(p => p.DesiredDateTime)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .GreaterThan(DateTime.Now).WithMessage($"{{PropertyName}} must be greater {DateTime.Now}");
            
            RuleFor(p => p.ActuallUntil)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .GreaterThan(DateTime.Now).WithMessage($"{{PropertyName}} must be greater {DateTime.Now}");
        }
    }
    
    
    public class CreateRequestCommand : IRequest<Guid>
    {
        public string Description { get; set; }
        public string Subject { get; set; }
        public Guid SubjectId { get; set; }
        public Guid StudentId { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ActuallUntil { get; set; }
        public DateTime DesiredDateTime { get; set; }
        public RequestStatus Status { get; set; }

        public class CreateRequestCommandHandler : IRequestHandler<CreateRequestCommand, Guid>
        {
            private readonly IRequestRepository _requestRepository;
            private readonly IEmailService _emailService;
            
            //private readonly IRequestClient<UserInfoRequest> _requestClient;

            public CreateRequestCommandHandler(IRequestRepository requestRepository,
                IEmailService emailService)
            {
                _requestRepository = requestRepository;
                _emailService = emailService;
            }

            public async Task<Guid> Handle(CreateRequestCommand command, CancellationToken cancellationToken)
            {
                var Request = new Request
                {
                    CreationDate = DateTime.Now,
                    Description = command.Description,
                    DesiredDateTime = command.DesiredDateTime,
                    SubjectId = command.SubjectId,
                    StudentId = command.StudentId,
                    Subject = command.Subject
                };

                Request.ActuallUntil = Request.DesiredDateTime.AddMonths(1);
                Request.Status = command.Status;
                Guid requestId = await _requestRepository.CreateAsync(Request);

                // var request = new UserInfoRequest
                // {
                //     id = Request.StudentId
                // };
                //var data = await _requestClient.GetResponse<FakeUserInfoResponse>(request, cancellationToken);
                
                await _emailService.SendEmailAsync("Новая заявка от Jumping Study Platform",
                    "Вы успешно создали заявку, скоро преподаватели откликнутся на нее и долгожданное обучение начнется.");
                
                //var request = new RestRequest("api/Email", Method.POST);
                //request.RequestFormat = DataFormat.Json;
                //request.AddJsonBody(new Email
                //{
                //    Address = "Lexs200610@yandex.ru",
                //    Subject = "Новая заявка",
                //    Body = "Поздравляем Вы успешно создали заявку на портале Jumping"
                //});
                //_notificationService.Execute(request);

                return requestId;
            }
        }
    }
}
