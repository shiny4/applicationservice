﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Entities.Enums;
using Application.Entities.Models;
using Application.Infrastructure.Interfaces;
using Application.Infrastructure.Interfaces.Services;
using Application.UseCases.Exceptions;
using FluentValidation;
using MediatR;

namespace Application.UseCases.Handlers.Requests.Commands
{
    
    public class CancelRequestCommandValidator : AbstractValidator<CancelRequestCommand>
    {
        public CancelRequestCommandValidator()
        {
            RuleFor(p => p.RequestId)
                .NotEmpty().WithMessage("{PropertyName} is required.");

            RuleFor(p => p.StudentId)
                .NotEmpty().WithMessage("{PropertyName} is required.");
        }
    }
    
    public class CancelRequestCommand : IRequest<Guid>
    {
        public Guid RequestId { get; set; }
        public Guid StudentId { get; set; }

        public class CancelRequestCommandHandler : IRequestHandler<CancelRequestCommand, Guid>
        {
            private readonly IRequestRepository _requestRepository;
            private readonly IEmailService _emailService;

            public CancelRequestCommandHandler(IRequestRepository requestRepository, IEmailService emailService)
            {
                _requestRepository = requestRepository;
                _emailService = emailService;
            }

            public async Task<Guid> Handle(CancelRequestCommand command, CancellationToken cancellationToken)
            {
                var request = await _requestRepository.GetRequestForUpdateAsync(command.RequestId);

                if (request == null)
                {
                    throw new UseCasesException($"Request with {command.RequestId} wasn't found", 404);
                }
                else
                {
                    if (request.StudentId != command.StudentId)
                    {
                        throw new UseCasesException($"Request with {command.RequestId} may be cancelled only by auther", 403);
                    }
                    request.Status = RequestStatus.Canceled;

                    await _requestRepository.UpdateAsync(request);
                    await _emailService.SendEmailAsync("Отмена заявки",
                        $"Заявка с ид={request.Id} отменена" );
                    
                    return request.Id;
                }
            }
        }
    }
}