﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Entities.Enums;
using Application.Entities.Models;
using Application.Infrastructure.Interfaces;
using Application.Infrastructure.Interfaces.Services;
using Application.UseCases.Exceptions;
using FluentValidation;
using MediatR;

namespace Application.UseCases.Handlers.Requests.Commands
{
    public class TakeRequestByTeacherCommandValidator : AbstractValidator<TakeRequestByTeacherCommand>
    {
        public TakeRequestByTeacherCommandValidator()
        {
            RuleFor(p => p.RequestId)
                .NotEmpty().WithMessage("{PropertyName} is required.");

            RuleFor(p => p.TeacherId)
                .NotEmpty().WithMessage("{PropertyName} is required.");
        }
    }

    public class TakeRequestByTeacherCommand : IRequest<Guid>
    {
        public Guid RequestId { get; set; }
        public Guid TeacherId { get; set; }

        public class TakeRequestByTeacherCommandHandler : IRequestHandler<TakeRequestByTeacherCommand, Guid>
        {
            private readonly IRequestRepository _requestRepository;
            private readonly IEmailService _emailService;

            public TakeRequestByTeacherCommandHandler(IRequestRepository requestRepository, IEmailService emailService)
            {
                _requestRepository = requestRepository;
                _emailService = emailService;
            }

            public async Task<Guid> Handle(TakeRequestByTeacherCommand command, CancellationToken cancellationToken)
            {
                var request = await _requestRepository.GetRequestForUpdateAsync(command.RequestId);

                if (request == null)
                {
                    throw new UseCasesException($"Request with {command.RequestId} wasn't found", 404);
                }
                else
                {
                    request.TeacherId = command.TeacherId;
                    request.Status = RequestStatus.Closed;

                    await _requestRepository.UpdateAsync(request);

                    await _emailService.SendEmailAsync("Подтверждение заявки",
                        $"Заявка с ид={request.Id} подтверждена" );

                    return request.Id;
                }
            }
        }
    }
}