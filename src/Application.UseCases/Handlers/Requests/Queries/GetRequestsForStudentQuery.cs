﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Entities.Models;
using Application.Infrastructure.Interfaces;
using Application.UseCases.Exceptions;
using FluentValidation;
using MediatR;

namespace Application.UseCases.Handlers.Requests.Queries
{
    public class GetRequestsForStudentQueryValidator : AbstractValidator<GetRequestsForStudentQuery>
    {
        public GetRequestsForStudentQueryValidator()
        {
            RuleFor(p => p.StudentId)
                .NotEmpty().WithMessage("{PropertyName} is required.");
        }
    }
    public class GetRequestsForStudentQuery : IRequest<ICollection<Request>>
    {
        public Guid StudentId { get; set; }

        public class GetRequestsForStudentQueryHandler : IRequestHandler<GetRequestsForStudentQuery, ICollection<Request>>
        {
            private readonly IRequestRepository _requestRepository;

            public GetRequestsForStudentQueryHandler(IRequestRepository requestRepository)
            {
                _requestRepository = requestRepository;
            }
            public async Task<ICollection<Request>> Handle(GetRequestsForStudentQuery query, CancellationToken cancellationToken)
            {
                var requests = await _requestRepository.GetRequestsForStudent(query.StudentId);
                if (requests == null||requests.Count==0) throw new UseCasesException($"Requests For Student with Id={query.StudentId} weren't found", 404);
                return requests;
            }
        } 
    }
}
