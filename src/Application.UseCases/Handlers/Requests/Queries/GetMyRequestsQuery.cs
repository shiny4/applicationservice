﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Entities.Models;
using Application.Infrastructure.Interfaces;
using Application.UseCases.Exceptions;
using FluentValidation;
using MediatR;

namespace Application.UseCases.Handlers.Requests.Queries
{
    
    public class GetMyRequestsQueryValidator : AbstractValidator<GetMyRequestsQuery>
    {
        public GetMyRequestsQueryValidator()
        {
            RuleFor(p => p.StudentId)
                .NotEmpty().WithMessage("{PropertyName} is required.");
        }
    }
    
    public class GetMyRequestsQuery : IRequest<IEnumerable<Request>>
    {
        public Guid StudentId { get; set; }

        public class GetMyRequestsQueryHandler : IRequestHandler<GetMyRequestsQuery, IEnumerable<Request>>
        {
            private readonly IRequestRepository _requestRepository;

            public GetMyRequestsQueryHandler(IRequestRepository requestRepository)
            {
                _requestRepository = requestRepository;
            }
            public async Task<IEnumerable<Request>> Handle(GetMyRequestsQuery query, CancellationToken cancellationToken)
            {
                var requests = await _requestRepository.GetRequestsForStudent(query.StudentId);
                    if (requests == null|| !requests.Any()) throw new UseCasesException($"Requests For Student with Id={query.StudentId} weren't found", 404);
                    return requests;
            }
        }
    }
}
