﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Entities.Models;
using Application.Infrastructure.Interfaces;
using Application.UseCases.Exceptions;
using MediatR;

namespace Application.UseCases.Handlers.Requests.Queries
{
    public class GetAllRequestsQuery : IRequest<IEnumerable<Request>>
    {
        public class GetAllRequestQueryHandler : IRequestHandler<GetAllRequestsQuery, IEnumerable<Request>>
        {
            private readonly IRequestRepository _requestRepository;

            public GetAllRequestQueryHandler(IRequestRepository requestRepository)
            {
                _requestRepository = requestRepository;
            }
            public async Task<IEnumerable<Request>> Handle(GetAllRequestsQuery query, CancellationToken cancellationToken)
            {
                var requestList = await _requestRepository.GetAllAsync();
                if (requestList == null|| !requestList.Any())
                {
                    throw new UseCasesException($"Requests weren't found", 404);
                }
                
                return requestList;
            }
        }
    }
}
