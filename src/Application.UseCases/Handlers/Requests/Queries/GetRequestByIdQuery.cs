﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Entities.Models;
using Application.Infrastructure.Interfaces;
using Application.UseCases.Exceptions;
using FluentValidation;
using MediatR;

namespace Application.UseCases.Handlers.Requests.Queries
{
    public class GetRequestByIdQueryValidator : AbstractValidator<GetRequestByIdQuery>
    {
        public GetRequestByIdQueryValidator()
        {
            RuleFor(p => p.Id)
                .NotEmpty().WithMessage("{PropertyName} is required.");
        }
    }
    public class GetRequestByIdQuery : IRequest<Request>
    {
        public Guid Id { get; set; }

        public class GetRequestByIdQueryHandler : IRequestHandler<GetRequestByIdQuery, Request>
        {
            private readonly IRequestRepository _requestRepository;
            public GetRequestByIdQueryHandler(IRequestRepository requestRepository)
            {
                _requestRepository = requestRepository;
            }
            public async Task<Request> Handle(GetRequestByIdQuery query, CancellationToken cancellationToken)
            {
                var Request = await _requestRepository.GetByIdAsync(query.Id);
                if (Request == null) throw new UseCasesException($"Request with {query.Id} wasn't found", 404);
                return Request;
            }
        }
    }
}