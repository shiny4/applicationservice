﻿using System;
using Application.Entities.Models;
using Application.UseCases.Handlers.Requests.Dto;
using AutoMapper;

namespace Application.UseCases.Handlers.Requests.Mappings
{
    public class RequestMapping : Profile
    {
        public RequestMapping()
        {
            CreateMap<Request, RequestResponseDto>();
            CreateMap<CreateOrUpdateRequestDto, Request>();
        }
    }
}