﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.UseCases.Handlers.Requests.Dto;

namespace Application.UseCases.Handlers.Requests.Interfaces
{
    public interface IRequestService
    {
        Task<ICollection<RequestResponseDto>> GetAllRequests();
        Task<string> AddRequestAsync(CreateOrUpdateRequestDto request);
        Task RemoveRequestAsync(Guid id);
        Task UpdateRequestAsync(Guid id, CreateOrUpdateRequestDto request);
        Task<RequestResponseDto> GetRequestByID(Guid id);
    }
}
