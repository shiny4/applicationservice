﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Entities.Enums;

namespace Application.UseCases.Handlers.Requests.Dto
{
    public class CreateOrUpdateRequestDto
    {
        public string Description { get; set; }
        public Guid TeacherId { get; set; }
        public Guid StudentId { get; set; }
        public Guid? SubjectId { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ActuallUntil { get; set; }
        public string DesiredDateTime { get; set; }
        public RequestStatus Status { get; set; }
    }
}