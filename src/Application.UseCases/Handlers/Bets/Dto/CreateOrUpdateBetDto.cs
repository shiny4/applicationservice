﻿using System;

namespace Application.UseCases.Handlers.Bets.Dto
{
    public class CreateOrUpdateBetDto
    {
        public Guid RequestId { get; set; }
        public Guid AuthorId { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }
    }
}