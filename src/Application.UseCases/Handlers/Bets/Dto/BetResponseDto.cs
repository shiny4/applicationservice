﻿using System;
using Application.Entities.Enums;

namespace Application.UseCases.Handlers.Bets.Dto
{
    public class BetResponseDto
    {
        public Guid RequestId { get; set; }
        public Guid AuthorId { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public float Price { get; set; }
        public BetStatus Status { get; set; }
    }
}
