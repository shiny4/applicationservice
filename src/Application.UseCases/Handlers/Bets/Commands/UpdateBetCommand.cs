﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Entities.Enums;
using Application.Infrastructure.Interfaces;
using Application.Infrastructure.Interfaces.Services;
using Application.UseCases.Exceptions;
using FluentValidation;
using MediatR;

namespace Application.UseCases.Handlers.Bets.Commands
{
    
    public class UpdateBetCommandValidator : AbstractValidator<UpdateBetCommand>
    {
        public UpdateBetCommandValidator()
        {
            RuleFor(p => p.Id)
                .NotEmpty().WithMessage("{PropertyName} is required.");

            RuleFor(p => p.Description)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull().WithMessage("{PropertyName} is required.")
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.")
                .MinimumLength(3).WithMessage("{PropertyName} must exceed 2 characters.");
            
            RuleFor(p => p.Price)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .GreaterThan(100f).WithMessage("{PropertyName} must be greater 100");
        }
    }
    public class UpdateBetCommand : IRequest<Guid>
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }

        public class UpdateBetCommandHandler : IRequestHandler<UpdateBetCommand, Guid>
        {
            private readonly IBetRepository _betRepository;
            private readonly IEmailService _emailService;
            public UpdateBetCommandHandler(IBetRepository betRepository, IEmailService emailService)
            {
                _betRepository = betRepository;
                _emailService = emailService;
            }
            public async Task<Guid> Handle(UpdateBetCommand command, CancellationToken cancellationToken)
            {
                var bet = await _betRepository.GetBetForUpdateAsync(command.Id);
                if (bet == null)
                {
                    throw new UseCasesException($"Bet with {command.Id} wasn't found", 404);
                }
                else
                {
                    bet.Description = command.Description;
                    bet.Price = command.Price;

                    await _betRepository.UpdateAsync(bet);
                    await _emailService.SendEmailAsync("Редактирование ставки",
                        $"Ставка с ид={bet.Id} изменена" );
                    
                    return bet.Id;
                }
            }
        }
    }

}