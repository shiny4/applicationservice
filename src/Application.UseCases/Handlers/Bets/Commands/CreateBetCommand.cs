﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Entities.Enums;
using Application.Entities.Models;
using Application.Infrastructure.Interfaces;
using Application.Infrastructure.Interfaces.Services;
using FluentValidation;
using MediatR;

namespace Application.UseCases.Handlers.Bets.Commands
{
    
    public class CreateBetCommandValidator : AbstractValidator<CreateBetCommand>
    {
        public CreateBetCommandValidator()
        {
            RuleFor(p => p.RequestId)
                .NotEmpty().WithMessage("{PropertyName} is required.");

            RuleFor(p => p.AuthorId)
                .NotEmpty().WithMessage("{PropertyName} is required.");
            
            RuleFor(p => p.Description)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull().WithMessage("{PropertyName} is required.")
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.")
                .MinimumLength(3).WithMessage("{PropertyName} must exceed 2 characters.");
            
            RuleFor(p => p.Price)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .GreaterThan(100f).WithMessage("{PropertyName} must be greater 100");
        }
    }
    public class CreateBetCommand : IRequest<Guid>
    {
        public Guid RequestId { get; set; }
        public Guid AuthorId { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }

        
        public class CreateBetCommandHandler : IRequestHandler<CreateBetCommand, Guid>
        {
            private readonly IBetRepository _betRepository;
            private readonly IEmailService _emailService;

            public CreateBetCommandHandler(IBetRepository betRepository, IEmailService emailService)
            {
                _betRepository = betRepository;
                _emailService = emailService;
            }

            public async Task<Guid> Handle(CreateBetCommand command, CancellationToken cancellationToken)
            {
                var Bet = new Bet
                {
                    RequestId = command.RequestId,
                    AuthorId = command.AuthorId,
                    Description = command.Description,
                    Price = command.Price,
                    CreationDate = DateTime.Now,
                };

                Bet.Status = BetStatus.Active;
                Guid betId = await _betRepository.CreateAsync(Bet);
                await _emailService.SendEmailAsync("Новая ставка от Jumping Study Platform",
                    "Вы успешно создали ставку.");

                return betId;
            }
        }
    }
}