﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Infrastructure.Interfaces;
using Application.Infrastructure.Interfaces.Services;
using Application.UseCases.Exceptions;
using FluentValidation;
using MediatR;

namespace Application.UseCases.Handlers.Bets.Commands
{
    public class DeleteBetByIdCommandValidator : AbstractValidator<DeleteBetByIdCommand>
    {
        public DeleteBetByIdCommandValidator()
        {
            RuleFor(p => p.Id)
                .NotEmpty().WithMessage("{PropertyName} is required.");
        }
    }
    public class DeleteBetByIdCommand : IRequest<Guid>
    {
        public Guid Id { get; set; }

        public class DeleteBetByIdCommandHandler : IRequestHandler<DeleteBetByIdCommand, Guid>
        {
            private readonly IBetRepository _betRepository;
            private readonly IEmailService _emailService;
            public DeleteBetByIdCommandHandler(IBetRepository betRepository, IEmailService emailService)
            {
                _betRepository = betRepository;
                _emailService = emailService;
            }
            public async Task<Guid> Handle(DeleteBetByIdCommand command, CancellationToken cancellationToken)
            {
                var bet = await _betRepository.GetByIdAsync(command.Id);

                if (bet == null) throw new UseCasesException($"Bet with {command.Id} wasn't found", 404);
                await _betRepository.DeleteAsync(bet);
                await _emailService.SendEmailAsync("Удаление ставки",
                    $"Ставка с ид={bet.Id} удалена" );
                return bet.Id;
            }
        }
    }
}