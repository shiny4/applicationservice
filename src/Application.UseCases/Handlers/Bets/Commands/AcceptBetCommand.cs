﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Entities.Enums;
using Application.Infrastructure.Interfaces;
using Application.Infrastructure.Interfaces.Services;
using Application.UseCases.Exceptions;
using FluentValidation;
using MediatR;

namespace Application.UseCases.Handlers.Bets.Commands
{
    public class AcceptBetCommandValidator : AbstractValidator<AcceptBetCommand>
    {
        public AcceptBetCommandValidator()
        {
            RuleFor(p => p.Id)
                .NotEmpty().WithMessage("{PropertyName} is required.");
        }
    }
    public class AcceptBetCommand : IRequest<Guid>
    {
        public Guid Id { get; set; }

        public class AcceptBetCommandHandler : IRequestHandler<AcceptBetCommand, Guid>
        {
            private readonly IBetRepository _betRepository;
            private readonly IRequestRepository _requestRepository;
            private readonly IEmailService _emailService;
            public AcceptBetCommandHandler(IBetRepository betRepository, IRequestRepository requestRepository, IEmailService emailService)
            {
                _betRepository = betRepository;
                _requestRepository = requestRepository;
                _emailService = emailService;
            }
            public async Task<Guid> Handle(AcceptBetCommand command, CancellationToken cancellationToken)
            {
                var bet = await _betRepository.GetBetForUpdateAsync(command.Id);
                if (bet == null)
                {
                    throw new UseCasesException($"Bet with {command.Id} wasn't found", 404);
                }
                else
                {
                    var request = await _requestRepository.GetRequestForUpdateAsync(bet.RequestId);

                    bet.Status = BetStatus.Accepted;

                    request.Price = bet.Price;
                    request.Status = RequestStatus.Closed;

                    await _betRepository.UpdateAsync(bet);
                    await _requestRepository.UpdateAsync(request);
                    await _emailService.SendEmailAsync("Подтверждение ставки",
                        $"Ставка с ид={request.Id} подтверждена" );
                    return bet.Id;
                }
            }
        }
    }

}
