﻿using System;
using System.Threading.Tasks;
using Application.UseCases.Handlers.Bets.Dto;

namespace Application.UseCases.Handlers.Bets.Interfaces
{
    public interface IBetService
    {
        Task<string> AddBetAsync(CreateOrUpdateBetDto request);
        Task RemoveBetAsync(Guid id);
        Task UpdateBetAsync(Guid id, CreateOrUpdateBetDto request);
        Task<BetResponseDto> GetBetByID(Guid id);
    }
}