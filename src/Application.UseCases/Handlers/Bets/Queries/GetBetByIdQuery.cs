﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Entities.Models;
using Application.Infrastructure.Interfaces;
using Application.UseCases.Exceptions;
using FluentValidation;
using MediatR;

namespace Application.UseCases.Handlers.Bets.Queries
{
    public class GetBetByIdQueryValidator : AbstractValidator<GetBetByIdQuery>
    {
        public GetBetByIdQueryValidator()
        {
            RuleFor(p => p.Id)
                .NotEmpty().WithMessage("{PropertyName} is required.");
        }
    }
    public class GetBetByIdQuery : IRequest<Bet>
    {
        public Guid Id { get; set; }

        public class GetBetByIdQueryHandler : IRequestHandler<GetBetByIdQuery, Bet>
        {
            private readonly IBetRepository _betRepository;

            public GetBetByIdQueryHandler(IBetRepository betRepository)
            {
                _betRepository = betRepository;
            }
            public async Task<Bet> Handle(GetBetByIdQuery query, CancellationToken cancellationToken)
            {
                var Bet = await _betRepository.GetByIdAsync(query.Id);
                if (Bet == null) throw new UseCasesException($"Bet with {query.Id} wasn't found", 404);
                return Bet;
            }
        }
    }
}