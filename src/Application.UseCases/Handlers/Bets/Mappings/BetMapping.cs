﻿using Application.Entities.Models;
using Application.UseCases.Handlers.Bets.Dto;
using AutoMapper;

namespace Application.UseCases.Handlers.Bets.Mappings
{
    public class BetMapping : Profile
    {
        public BetMapping()
        {
            CreateMap<Bet, BetResponseDto>();
            CreateMap<CreateOrUpdateBetDto, Bet>();
        }
    }
}