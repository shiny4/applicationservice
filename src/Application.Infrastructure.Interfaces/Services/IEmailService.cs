using System;
using System.Threading.Tasks;

namespace Application.Infrastructure.Interfaces.Services
{
    public interface IEmailService
    {
        Task SendEmailAsync(string subject, string body,string address="");
    }
}