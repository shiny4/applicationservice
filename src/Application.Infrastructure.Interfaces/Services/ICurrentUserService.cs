using System;

namespace Application.Infrastructure.Interfaces.Services
{
    public interface ICurrentUserService
    {
        Guid UserId { get; }
        bool IsAuthenticated { get; }
        string Email { get; }
        string Name { get; }
        string Role { get; }
    }
}