﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Entities.Models;

namespace Application.Infrastructure.Interfaces
{
    public interface IBetRepository : IBaseRepository<Bet>
    {
        Task<Bet> GetBetForUpdateAsync(Guid id);
        Task<ICollection<Bet>> GetBetsForRequest(Guid Id);
    }
}
