﻿using System;
namespace Jumping.Contracts
{
    public interface IUserInfoRequest
    {
        public Guid id { get; set; }
    }

    public class UserInfoRequest : IUserInfoRequest
    {
        public Guid id { get; set; }
    }

    public interface IUserInfoResponse
    {
        public Guid id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class FakeUserInfoResponse : IUserInfoResponse
    {
        public Guid id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
