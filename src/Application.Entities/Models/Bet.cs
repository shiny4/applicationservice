﻿using System;
using Application.Entities.Enums;

namespace Application.Entities.Models
{
    public class Bet : BaseEntity
    {
        public Guid RequestId { get; set; }
        public Guid AuthorId { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public float Price { get; set; }
        public BetStatus Status { get; set; }
    }
}
